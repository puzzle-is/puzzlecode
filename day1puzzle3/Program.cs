﻿using System;

namespace Puzzle3
{
     class Program
    {
        static int MAX = 100001;
         static int MinRoom(int[ , ] lectures,int n)
        {
            int [] ongoing = new int[MAX];

            for(int i = 0 ; i<n ; i++)
            {
                ongoing[lectures[i,0]]++;
                ongoing[lectures[i,1] + 1]--;
            }

            int ans = ongoing[0];


            for(int i = 1; i<MAX ; i++)
            {
                ongoing[i] += ongoing[i - 1];
                ans = Math.Max(ans ,ongoing[i]);
            }
            return ans;
        }

         public  static void Main(String[] args)
        {
            int [ , ] lectures = {{0, 75}, {0, 50}, {60, 150}};
            int n = lectures.GetLength(0);
            Console.WriteLine(MinRoom(lectures , n));
        }
    }
}
