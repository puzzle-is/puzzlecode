﻿using System;

namespace day3puzzle5
{
    class Program
    {
        private static void permutation(String str, int l, int r) 
    { 
        if (l == r) 
            Console.WriteLine(str); 
        else 
        { 
            for (int i = l; i <= r; i++) 
            { 
                str = swap(str, l, i); 
                permutation(str, l + 1, r); 
                str = swap(str, l, i); 
            } 
        } 
    } 
  
    /** 
     Swap Characters at position 
     a string value 
     i position 1 
     j position 2 
     swapped string 
    */ 
    public static String swap(String a, int i, int j) 
    { 
        char temp; 
        char[] charArray = a.ToCharArray(); 
        temp = charArray[i] ; 
        charArray[i] = charArray[j]; 
        charArray[j] = temp; 
        string s = new string(charArray); 
        return s; 
    } 
  
//  Code 
public static void Main() 
{ 
    String str = "XYZ"; 
    int n = str.Length; 
    permutation(str, 0, n-1); 
} 
} 
 

    }

