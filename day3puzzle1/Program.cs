﻿// See https://aka.ms/new-console-template for more information
//Console.WriteLine("Hello, World!");
using System;
class day3puzzle1
{
    static void Main (String[] args)
    {
        LinkedList list = new LinkedList();
        list.AddNode(new LinkedList.Node(65));
        list.AddNode(new LinkedList.Node(45));
        list.AddNode(new LinkedList.Node(20));
        list.AddNode(new LinkedList.Node(30));
        Console.WriteLine("Linked list is:");
        list.PrintList();

        list.ReverseList();

        Console.WriteLine("Linked list after reversal is:");
        list.PrintList();

    }

}

class LinkedList
{
    Node head;

    public class Node
    {
        public int data;
        public Node next;
        public Node (int d)
        {
            data = d;
            next = null;
        }
    }

    public void AddNode(Node node)
    {
        if(head == null)
        head = node;
        else
        {
            Node temp = head;
            while(temp.next != null)
            {
            temp = temp.next;
            }
            temp.next = node;

        }

        
    }



public void ReverseList()
{
    Node prev = null,current = head ,next =null;
    while(current!=null)
    {
        next = current.next;
        current.next = prev;
        prev = current;
        current = next;
    }
    head = prev;
}

public void PrintList()
{
    Node current = head;
    while(current!=null)
    {
        Console.WriteLine(current.data + " ");
        current = current.next;
    }
    Console.WriteLine();
}
}

 