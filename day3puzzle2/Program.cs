﻿using System;
class BT
{
    public class node
    {
         public int data;
         public node left , right;

         public node(int key)
         {
             data = key;
             left = right = null;
                
         }
    }
    
    static int maxLevel = -1;
    static int res = -1;
    
    static void find(node root , int level)
    {
        if(root != null)
        {
            find(root.left, ++level);
            if(level>maxLevel)
            {
                 res = root.data;
                 maxLevel = level;
            }
        find(root.right,level);

        }
    }
    
    static int deepestNode(node root)
    {
        find(root , 0);
        return res;
    }
    public static void Main(String[] args)
    {
        node root = new node(1);
        root.left = new node(2);
        root.right = new node(3);
        root.left.left = new node(4);
        root.right.right = new node(5);
        root.left.left.right = new node(6);
        Console.WriteLine(deepestNode(root));
    }
//}
}